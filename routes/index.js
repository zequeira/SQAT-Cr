var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/success/:successCode', function(req, res, next) {
    res.render('index', { successCode: req.params.successCode });
});

router.get('/fail/', function (req, res) {
    res.render('qcFail');
});

module.exports = router;
