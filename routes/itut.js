var express = require('express');
var router = express.Router();
var MongoClient = require('mongodb').MongoClient;

var usersNoStimuli = require('../data/usersNoStimuli.json');

var url = 'mongodb://localhost:27017/itut';
var environment = (process.env.NODE_ENV == 'production') ? 'vm002.qu.tu-berlin.de' : 'localhost:9000';

var connectMongoDB = function (callback) {
    MongoClient.connect(url, function(err, db) {
        if (err) throw err;
        console.log("Connected correctly to server");

        callback(db);
    });
};

var insertData = function(db, collection, data, callback) {
    db.collection(collection).insertOne(data, function(err, result) {
        if (err) throw err;
        callback(result);
    });
};

var updateWorker = function (db, workerID, entryObj) {
    db.collection('worker15').updateOne({"_id": workerID}, {$set: entryObj}, function(err, result) {
        if (err) throw err;
        console.log(result);
    });
};

router.post('/user', function (req, res) {
    connectMongoDB(function (db) {
        if (usersNoStimuli[req.body.userID]) {
            connectMongoDB(function (db) {
                insertData(db, 'qualifikation', {"userID": req.body.userID, "userIP": req.body.userIP}, function (answer) {db.close();});
            });
            res.json({'status': false, 'oldUser': true});
        } else {
            db.collection('users').findOne({"_id": req.body.userID}, function(err, user) {
                if (err) throw err;

                if (!user) {
                    // then first time the user come, put it in DB
                    let entryObj = {"_id":req.body.userID, "timeStamp": Date.now(), "userIP": req.body.userIP, "count": 1};
                    insertData(db, 'users', entryObj, function (answer) {db.close();});

                    let ip4RegExp = /\d+\.\d+\.\d+\.\d+/gmi;
                    let ip6RegExp = /\w+\:\w+\:\w+\:\w+\:\w+\:\w+\:\w+\:\w+/gmi;
                    let ip4 = ip4RegExp.exec(req.body.userIP);
                    let ip6 = ip6RegExp.exec(req.body.userIP);

                    if (req.body.userIP == undefined || req.body.userIP == null || req.body.userIP == "") {
                        res.json({'status': true, 'oldUser': false});
                    } else if (ip4 || ip6) {
                        db.collection('qualifikation').findOne({"userIP": req.body.userIP}, (err, resultByIp) => {
                            if (err) throw err;

                            if (resultByIp) {
                                // user already participated, CAN NOT CONTINUE !!
                                res.json({'status': false, 'oldUser': false});
                            } else {
                                // then let the user try it once more
                                res.json({'status': true, 'oldUser': false});
                            }
                        });
                    }
                } else {
                    db.collection('users').updateOne({"_id": req.body.userID}, {$set:{count: user.count+1}});
                    // check if user exist in "qualifikation" DB
                    db.collection('qualifikation').findOne({"userID": req.body.userID}, function(err, result) {
                        if (err) throw err;

                        if (result) {
                            // user already participated, CAN NOT CONTINUE !!
                            res.json({'status': false, 'oldUser': false});
                        } else {
                            let ip4RegExp = /\d+\.\d+\.\d+\.\d+/gmi;
                            let ip6RegExp = /\w+\:\w+\:\w+\:\w+\:\w+\:\w+\:\w+\:\w+/gmi;
                            let ip4 = ip4RegExp.exec(req.body.userIP);
                            let ip6 = ip6RegExp.exec(req.body.userIP);

                            if (req.body.userIP == undefined || req.body.userIP == null || req.body.userIP == "") {
                                res.json({'status': true, 'oldUser': false});
                            } else if (ip4 || ip6) {
                                db.collection('qualifikation').findOne({"userIP": req.body.userIP}, (err, resultByIp) => {
                                    if (err) throw err;

                                    if (resultByIp) {
                                        // user already participated, CAN NOT CONTINUE !!
                                        res.json({'status': false, 'oldUser': false});
                                    } else {
                                        // then let the user try it once more
                                        res.json({'status': true, 'oldUser': false});
                                    }
                                });
                            }
                        }
                    });
                }

            });
        }
    });
});


router.get('/qualifikation', function(req, res, next) {

    /*
    console.log("ñlwekd´ñewf wefk´ñewfwe");
    var dataStream = require('../data/workers.temp.json');

    console.log(dataStream);

    let newJSON = {};
    let z = 0;

    for (var i=1; i<=59; i++) {
        for (var j=1; j<=20; j++) {
            newJSON['id'+j] = dataStream
            z++;
        }
    }
    */

    res.render('qualifikation', {environment: environment});
});

router.post('/qualifikation', function (req, res) {
    let results = req.body;
    results['trapSF'] = ((results.trap1 == 2) && (results.trap2 == 3) && (results.trap3 == 1)) ? "SUCCESS" : "FAIL";
    let code = ((results.trap1 == 2) && (results.trap2 == 3) && (results.trap3 == 1)) ? "5S9vvCpURw4ban74" : null;
    connectMongoDB(function (db) {
        insertData(db, 'qualifikation', results, function (answer) {
            console.log(answer);
            res.json({'result': results['trapSF'], 'code': code});
            db.close();
        });
    });
});

router.get('/ausbildung', function(req, res, next) {
    res.render('ausbildung');
});

router.post('/ausbildung', function (req, res) {
    let ausbildungData = req.body;
    connectMongoDB(function (db) {
        insertData(db, 'ausbildung', ausbildungData, function (answer) {db.close();});

        /*
        let entryObj = {};
        if (ausbildungData['trapMathTF']) {
            entryObj = {"timeStamp": Date.now(), "ausbildung": false};
        } else if (!ausbildungData['trapMathTF']) {

        }
        */

        let entryObj = {"timeStamp": Date.now(), "ausbildung": ausbildungData['trapMathTF'] ? false : null};
        updateWorker(db, ausbildungData['userID'], entryObj);
        //res.json({'ausbildung': ausbildungData['trapMathTF']});
        // the "status" property is for nothing
        res.json({'ausbildung': entryObj.ausbildung, 'status': false});
    });
});

router.get('/bewertung', function(req, res, next) {
    res.render('bewertung');
});


module.exports = router;
