var express = require('express');
var router = express.Router();
var MongoClient = require('mongodb').MongoClient;

// Connection URL
var url = 'mongodb://localhost:27017/itut';

var additionalCodesGroup1 = ["pEWWmAnTC2R9YFsm","hE5N657XM85ZC4tN","ZayqR9qPchUngAhw","V6vq6xhd747jMi69","972R4e9vnSnPaB99"];

var connectMongoDB = function (callback) {
    MongoClient.connect(url, function(err, db) {
        if (err) throw err;
        console.log("Connected correctly to server");

        callback(db);
    });
};

var insertResults = function(db, group, data, callback) {
    let collection = db.collection('results'+group);
    // Insert some documents
    collection.insertMany(data, function(err, result) {
        if (err) throw err;
        callback(result);
    });
};

var updateWorker = function (db, workerID, entryObj) {
    db.collection('worker15').updateOne({"_id": workerID}, {$set: entryObj}, function(err, result) {
        if (err) throw err;
        console.log(result);
    });
};

var getWorkerCounterAndCode = function (db, collectionDB, workerID, orderNo, results, callback) {

    let failQC = false;
    for (let i=0; i<results.length; i++) {
        if (results[i].trapQxSF == 'FAIL') {
            failQC = true;
            break;
        }
    }
    if (failQC) {

        let entryObj = {"timeStamp": Date.now(), "ausbildung": failQC ? null : false};

        //let entryObj = {"timeStamp": Date.now(), "ausbildung": null};
        updateWorker(db, workerID, entryObj);

        callback(null);
    } else {
        let jobsGroup = require('../data/jobsGroup'+collectionDB+'.json');
        let id = workerID+'-'+orderNo;
        id = (parseInt(orderNo) > 15) ? orderNo : id;

        callback(jobsGroup[id].code);
    }
};

router.post('/:group', function(req, res, next) {
    console.log(Date.now());
    connectMongoDB(function (db) {
        insertResults(db, req.params.group, req.body.results, function (result) {
            // update worker timeStamp
            //updateWorker(db, req.params.group, req.body.results[0].user, {timeStamp: Date.now()});


            getWorkerCounterAndCode(db, req.params.group, req.body.results[0].user, req.body.order, req.body.results, function (code) {
                console.log(result);
                res.json({'result': result, 'code': code});
                db.close();
            });
        });
    });
});

module.exports = router;