var express = require('express');
var router = express.Router();
var MongoClient = require('mongodb').MongoClient;

var jobsGroup10 = require('../data/jobsGroup15_new.json');

var jobsGroup10Full = require('../data/jobsGroup15.json');

// Connection URL
var url = 'mongodb://localhost:27017/itut';

/*
var usersGroup1 = [
    642747,
    1453213,
    450969,
    896322,
    1495170,
    69158,
    1066986,
    584510,
    1115212,
    314824,
    1175676,
    1469016,
    1447621,
    876686,
    1105263,
    1508172,
    816849
];
*/



var connectMongoDB = function (callback) {
    MongoClient.connect(url, function(err, db) {
        if (err) throw err;
        console.log("Connected correctly to server");

        callback(db);
    });
};

var insertWorker = function(db, collectionDB, obj, callback) {
    let collection = db.collection('worker'+collectionDB);
    // Insert some documents
    collection.insertOne(obj, function(err, result) {
        if (err) throw err;
        callback(result);
    });
};

var updateWorker = function (db, collectionDB, workerID, entryObj, callback) {
    let collection = db.collection('worker'+collectionDB);
    collection.updateOne({"_id": workerID}, {$set: entryObj}, function(err, result) {
        if (err) throw err;
        console.log(result);
        callback(result);
    });
};



let getProgress = (db, collectionDB, workerID) => {
    return new Promise((resolve, reject) => {
        db.collection('worker'+collectionDB+'first').findOne({"_id": workerID}, (err, result) => {
            if (err) {
                return reject(err);
            } else {
                return resolve( (result) ? result.progress : result);
            }
        });
    });
};


let checkProgress = (db, collectionDB, result, callback) => {
    return getProgress(db, collectionDB, result._id).then((progress) => {
        console.log(progress);
        if (progress) {
            return progress;
        } else {
            if (result.parentUser) {
                return getProgress(db, collectionDB, result.parentUser.toString());
            }
        }
    }).then((progress) => {
        callback(progress);
    }).catch((err) => {
        console.log(err);
    });
};



var findWorker = function (db, collectionDB, workerID, callback) {
    let collection = db.collection('worker'+collectionDB);
    // Insert some documents
    collection.findOne({"_id": workerID}, function(err, result) {
        if (err) throw err;

        if (result != null) {

            checkProgress(db, collectionDB, result, function(progress) {
                let done = false;
                if (collectionDB == 15) {
                    done = (progress >= 14) ? true : false;
                }

                if (done) {
                    callback(result, false, null, null, false, done);
                } else {
                    let count = result.count;
                    let currentTime = Date.now();
                    let elapsedTime = currentTime - result.timeStamp;
                    console.log(elapsedTime);

                    let minutes = elapsedTime / 60000;
                    console.log('MINUTES ARE: '+minutes);

                     if (minutes > 60) {
                        // can not continue -- Redirect to the Training/Ausbildung
                        callback(result, false, null, null, true, done);
                    }  else if (minutes < 60 & (result.ausbildung == false)) {
                        callback({}, true, minutes, count+1, false, done);
                    } else if (result.ausbildung == null) {
                        // worker failed the training, then bann for 1h

                        //if (!result.hasOwnProperty('ausbildung')) {
                        // can not continue with Training/Ausbildung
                        callback({}, false, minutes, null, null, done);
                    }

                    /*
                    else if (minutes < 60 & result.ausbildung) {
                        // then can continue and update Time Stamp
                        //updateWorker(db, collectionDB, workerID, currentTime, count+1, function (resultUpd, count) {
                        //updateWorker(db, collectionDB, workerID, {count: count+1}, function (resultUpd) { });
                        // can continue
                        callback({}, true, null, count+1, false, done);
                    }
                    */
                }
            });


        } else {
            //let entryObj = {"_id": workerID, "timeStamp": Date.now(), "ausbildung": false, "count": 1};
            let entryObj = {"_id": workerID, "timeStamp": Date.now(), "count": 1};
            insertWorker(db, collectionDB, entryObj, function (result) {
                console.log(result);
                //return res.redirect('/itut/ausbildung?itr='+order+'&user_id='+workerID);
                //callback(result, true, null, result.ops[0].count, true, false);
                callback(result, true, null, result.ops[0].count, true, false);
            });
        }
    });
};

router.post('/:collection', function(req, res, next) {
    console.log(Date.now());
    connectMongoDB(function (db) {
        findWorker(db, req.params.collection, req.body.workerID, function (result, status, minutes, count, ausbildung, done) {
            console.log(result);
            res.json({'status': status, 'minutes': minutes, 'count': count, 'ausbildung': ausbildung, 'done': done});
            db.close();
        });
    });
});

router.post('/data/:collection', function(req, res, next) {
    let id = req.body.workerID+'-'+req.body.count;
    let jobs;
    let jobsGroup;
    let jobsGroupFull;
    let usersGroup;
    if (req.params.collection == 15) {
        id = req.body.workerID+'-'+1;
        jobs = jobsGroup10[id];
        jobsGroup = jobsGroup10;
        jobsGroupFull = jobsGroup10Full;
        //usersGroup = usersGroup1;
    }
    if (jobs) {
        //res.json({'status': true, 'files': jobs.files, 'trapQx': jobs.trapQx});
        connectMongoDB(function (db) {
            db.collection('worker'+req.params.collection+'first').findOne({"_id": req.body.workerID}, function (err, result) {
                if (err) throw err;
                if (result != null) {
                    db.collection('worker'+req.params.collection+'first').updateOne({"_id": req.body.workerID}, {$set:{progress: parseInt(result.progress)+1}});
                    id = req.body.workerID+'-'+(parseInt(result.progress)+1);
                } else {
                    let entryObj = {"_id": req.body.workerID, "progress": req.body.count};
                    db.collection('worker'+req.params.collection+'first').insertOne(entryObj);
                    //id = req.body.workerID+'-2';
                }
                jobs = jobsGroup[id];
                res.json({'status': true, 'files': jobs.files});
            });
        });

    } else {

        if (jobsGroupFull[req.body.workerID+'-1']) {
            connectMongoDB(function (db) {

                db.collection('worker'+req.params.collection).findOne({"_id": req.body.workerID}, function (err, worker) {
                    if (err) throw err;
                    if (worker.parentUser) {
                        db.collection('worker'+req.params.collection+'first').findOne({"_id": (worker.parentUser).toString()}, function (err, result) {
                            if (err) throw err;
                            db.collection('worker'+req.params.collection+'first').updateOne({"_id": (worker.parentUser).toString()}, {$set:{progress: parseInt(result.progress)+1}});
                            id = worker.parentUser+'-'+(parseInt(result.progress)+1);
                            jobs = jobsGroup[id];
                            res.json({'status': true, 'files': jobs.files});
                            /*if (result != null) {
                             db.collection('worker'+req.params.collection+'first').updateOne({"_id": worker.parentUser}, {$set:{progress: parseInt(result.progress)+1}});
                             id = worker.parentUser+'-'+(parseInt(result.progress)+1);
                             } else {
                             let entryObj = {"_id": usersGroup[cont.count], "progress": 2, "users": [req.body.workerID]};
                             db.collection('worker'+req.params.collection+'first').insertOne(entryObj);
                             id = usersGroup[cont.count]+'-2';
                             }
                             jobs = jobsGroup[id];
                             res.json({'status': true, 'files': jobs.files, 'trapQx': jobs.trapQx});*/
                        });
                    } else {
                        // figure out which user number is next to get
                        db.collection('cont').findOne({"_id": 'cont'+req.params.collection}, function(err, cont) {
                            if (err) throw err;
                            db.collection('worker'+req.params.collection).updateOne({"_id": req.body.workerID}, {$set:{parentUser: usersGroup[cont.count]}});
                            let newCount = (cont.count < usersGroup.length-1) ? cont.count+1 : 0;
                            db.collection('cont').updateOne({"_id": 'cont'+req.params.collection}, {$set:{count: newCount}});

                            db.collection('worker'+req.params.collection+'first').findOne({"_id": (usersGroup[cont.count]).toString()}, function (err, result) {
                                if (err) throw err;
                                if (result != null) {
                                    if (result.users) {
                                        result.users.push(req.body.workerID);
                                    } else {
                                        result.users = [req.body.workerID];
                                    }
                                    db.collection('worker'+req.params.collection+'first').updateOne({"_id": (usersGroup[cont.count]).toString()}, {$set:{progress: parseInt(result.progress)+1, users: result.users}});
                                    id = usersGroup[cont.count]+'-'+(parseInt(result.progress)+1);
                                } else {
                                    let entryObj = {"_id": (usersGroup[cont.count]).toString(), "progress": 2, "users": [req.body.workerID]};
                                    db.collection('worker'+req.params.collection+'first').insertOne(entryObj);
                                    id = usersGroup[cont.count]+'-2';
                                }
                                jobs = jobsGroup[id];
                                res.json({'status': true, 'files': jobs.files});
                            });

                        });
                    }
                });



            });
        } else {
            res.json({'status': false});
        }

    }
});

router.get('/reset/:workerID', function(req, res, next) {
    let collection;
    let timeStamp = 1509971110750;
    let workerID = req.params.workerID;

    connectMongoDB(function (db) {
        if (req.params.workerID == '1580921') {
            collection = db.collection('worker10');
        } else if (req.params.workerID == '1580958') {
            collection = db.collection('worker20');
        } else if (req.params.workerID == '1506867') {
            collection = db.collection('worker40');
        }

        collection.findOne({"_id": workerID}, function(err, result) {
            if (err) throw err;

            if (result != null) {
                let count = result.count;

                collection.updateOne({"_id": workerID}, {$set:{timeStamp: timeStamp, count: count+1}}, function(err, result) {
                    if (err) throw err;
                    console.log(result);
                    res.json({'status': true});
                });
            }
        });
    });
});




module.exports = router;